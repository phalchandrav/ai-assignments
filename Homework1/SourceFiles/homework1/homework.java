package homework1;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.PriorityQueue;

public class homework {
	public static void main( String args[]) { 
		HashMap<String, Node> inputGraph;
		String data[], outputString = null;
		
		data = readFromFile("D:\\Users\\phalc\\workspace\\Homework\\Cases\\GitInput\\input.txt");
		inputGraph = Node.constructGraph(data);

		switch(data[0]){
		case "BFS":
			outputString = Node.performBFS(data[1], data[2], inputGraph);
//			System.out.println( "Output:"+ "\n" + outputString );
			break;
		case "DFS":
			outputString = Node.performDFS(data[1], data[2], inputGraph);
//			System.out.println( "Output:"+ "\n" + outputString );
			break;
		case "UCS":
			outputString = Node.performUCS(data[1], data[2], inputGraph);
//			System.out.println( "Output:"+ "\n" + outputString );
			break;
		case "A*":
			outputString = Node.performAStar(data[1], data[2], inputGraph);
//			System.out.println( "Output:"+ "\n" + outputString );
		}

		writeToFile("D:\\Users\\phalc\\workspace\\Homework\\Cases\\MyOutput\\output.txt", outputString);
		inputGraph = null;
		data = null;
		outputString = "";
	}
	
	public static String[] readFromFile(String fileName){
		BufferedReader br;
		ArrayList<String> input = null;
		String line, output[] = null;
		try{
			br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
			input = new ArrayList<>();
			line = br.readLine();
			while(line!=null){
				input.add(line);
				line = br.readLine();
			}
			br.close();
		} catch (IOException E){
			E.printStackTrace();
		}
		output = new String[input.size()];
		input.toArray(output);
		return output;
	}
	
	public static void writeToFile(String fileName, String data){
		OutputStreamWriter fileOutput;
		try{
			fileOutput = new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8");
			fileOutput.write(data);
			fileOutput.close();
		} catch (IOException E){
			E.printStackTrace();
		}
	}
}

final class Node{
	private LinkedHashMap<Node, Long> childrens;
	private PriorityQueue<Node> parent;
	private String nodeInfo;
	private long pathCost;
	private long heuristicsVal;
	private long depth;
	private long inputOrder;
	private boolean visited;
	public Node(String nodeInfo, long inputOrder){
		this.nodeInfo = nodeInfo;
		parent = new PriorityQueue<>(new Comparator<Node>() {

			@Override
			public int compare(Node o1, Node o2) {
				// TODO Auto-generated method stub
				if(o1.getInputOrder() < o2.getInputOrder()){
					return -1;
				} else if(o1.getInputOrder() == o2.getInputOrder()){
					return 0;
				} else{
					return 1;
				}
			}
		});
		childrens = new LinkedHashMap<>();
		visited = false;
		this.inputOrder = inputOrder;
	}
	
	@Override
	public String toString(){
		return nodeInfo;
	}
	
//	@Override
//	public boolean equals(Object obj){
//		if(obj instanceof Node){
//			Node otherNode = (Node) obj;
//			return (this.toString().equals(otherNode.toString()));
//		} else{
//			return false;
//		}
//	}
	
	public void addChild(Node child, long stepCost){
		childrens.put(child, stepCost);
	}
	
	public void addParent(Node parent){
//		this.parent = parent;
		this.parent.add(parent);
	}
	
	public void resetParents(){
		this.parent = null;
		this.parent = new PriorityQueue<>(new Comparator<Node>() {

			@Override
			public int compare(Node o1, Node o2) {
				// TODO Auto-generated method stub
				if(o1.getInputOrder() < o2.getInputOrder()){
					return -1;
				} else if(o1.getInputOrder() == o2.getInputOrder()){
					return 0;
				} else{
					return 1;
				}
			}
		});
	}
	
	public Iterator<Node> getChildrenIterator(){
//		return children.iterator();
		return childrens.keySet().iterator();
	}
	
	public void setDepth(long depth){
		this.depth = depth;
	}
	
	public boolean hasParent(){
		if(parent != null){
			return true;
		}else{
			return false;
		}
	}
	
	public Node getParent(){
		return parent.peek();
	}
	
	public long getDepth(){
		return this.depth;
	}
	
	public void setVisited(boolean state){
		this.visited = state;
	}
	
	public boolean isVisited(){
		return visited;
	}
	
	public long getPathCost(){
		return this.pathCost;
	}
	
	public void setPathCost(long pathCost){
		this.pathCost = pathCost;
	}
	
	public long getStepCost(Node child){
		return childrens.get(child);
	}
	
	public void setHeuristics(long heuristicsVal){
		this.heuristicsVal = heuristicsVal;
	}
	
	public long getHeuristics(){
		return this.heuristicsVal;
	}
	
	public long getInputOrder(){
		return this.inputOrder;
	}
	
	public static HashMap<String, Node> constructGraph( String input[]){
		HashMap<String, Node> inputGraph = new HashMap<>();
		Node parentNode, childNode;
		int noOfIterations, i=0, offset1, offset2, inputOrder = 1;
		String line[] = new String[3];
		noOfIterations = Integer.parseInt(input[3]);
		offset1 = 4;
		for(i=4; i<4+noOfIterations; i++){
			line = input[i].split("\\s");
			if(!inputGraph.containsKey(line[1])){
				childNode = new Node(line[1], inputOrder);
				inputGraph.put(line[1], childNode);
				inputOrder++;
			} else{
				childNode = inputGraph.get(line[1]);
			}
			if(inputGraph.containsKey(line[0])){
				parentNode = inputGraph.get(line[0]);
				parentNode.addChild(childNode, Long.parseLong(line[2]));
				
			} else {
				parentNode = new Node(line[0], inputOrder);
				parentNode.addChild(childNode, Long.parseLong(line[2]));
				inputGraph.put(line[0], parentNode);
				inputOrder++;
			}
		}
		offset2 = offset1+noOfIterations+1;
		if(offset1+noOfIterations < input.length){
			noOfIterations = Integer.parseInt(input[offset1+noOfIterations]);
			for(i=offset2; i<offset2+noOfIterations ;i++ ){
				line = input[i].split("\\s");
				if(inputGraph.containsKey(line[0])){
					parentNode = inputGraph.get(line[0]);
					parentNode.setHeuristics(Long.parseLong(line[1]));
				}
			}
		}
		return inputGraph;
	}
	
	public static String performBFS( String startState, String goalState, HashMap<String, Node> inputGraph){
		long depth = 0;
		StringBuilder sb = new StringBuilder();
		Node popNode, childNode, goalNode;
		LinkedList<Node> bfsQueue = new LinkedList<>();
		Iterator<Node> childIt;
		childNode = inputGraph.get(startState);
		childNode.setDepth(depth);
		bfsQueue.add(childNode);
		childNode = null;
//		System.out.println("Traverse:\n");
		while(!bfsQueue.isEmpty()){
			popNode = bfsQueue.remove();
			popNode.setVisited(true);
//			System.out.println(popNode.toString() + "\t" + popNode.getParent() + "\n");
			if(popNode.toString().equals(goalState)){
				goalNode = popNode;				
				while(goalNode != null){
					sb.insert(0, goalNode.toString()+" "+goalNode.getDepth()+"\n");
					goalNode = goalNode.getParent();
				}
				return sb.toString().trim();
			} else {
				depth = popNode.getDepth() + 1;
				childIt = popNode.childrens.keySet().iterator();
				while(childIt.hasNext()){
					childNode = childIt.next();
//					childNodeB = inputGraph.get(childNodeA.toString());
					if(!childNode.isVisited() && !bfsQueue.contains(childNode)){
						childNode.addParent(popNode);
						childNode.setDepth(depth);
						bfsQueue.add(childNode);
					}
				}
			}
		}
		return null;
	}
	
	public static String performDFS( String startState, String goalState, HashMap<String, Node> inputGraph){
		long depth = 0;
		int i = 0;
		StringBuilder sb = new StringBuilder();
		Node popNode, childNode, goalNode, queueNode;
		LinkedList<Node> dfsQueue = new LinkedList<>();
		Iterator<Node> childIt;
		childNode = inputGraph.get(startState);
		childNode.setDepth(depth);
		dfsQueue.add(childNode);
		childNode = null;
//		System.out.println("Traverse:\n");
		while(!dfsQueue.isEmpty()){
			i=0;
			popNode = dfsQueue.remove();
			popNode.setVisited(true);
//			System.out.println(popNode.toString() + "\t" + popNode.getParent() + "\n");
			if(popNode.toString().equals(goalState)){
				goalNode = popNode;				
				while(goalNode != null){
					sb.insert(0, goalNode.toString()+" "+goalNode.getDepth()+"\n");
					goalNode = goalNode.getParent();
				}
				return sb.toString().trim();
			} else {
				depth = popNode.getDepth() + 1;
				childIt = popNode.childrens.keySet().iterator();
				while(childIt.hasNext()){
					childNode = childIt.next();
					if(!childNode.isVisited()){
						if(dfsQueue.contains(childNode)){
							queueNode = dfsQueue.get(dfsQueue.indexOf(childNode));
//							if(queueNode.getDepth() < depth && childNode.toString().equals(goalState)){
							if(queueNode.getDepth() < depth){
								continue;
							}
						}
						childNode.addParent(popNode);
						childNode.setDepth(depth);
						dfsQueue.add(i, childNode);
						i++;
					}
				}
			}
		}
		return null;
	}
	
	public static String performUCS( String startState, String goalState, HashMap<String, Node> inputGraph){
		long pathCost = 0, pathCostQueueNode = 0, stepCost = 0;
		StringBuilder sb = new StringBuilder();
		Node popNode, childNode, goalNode;
		PriorityQueue<Node> ucsQueue = new PriorityQueue<>(new Comparator<Node>() {
			@Override
			public int compare(Node o1, Node o2) {
				// TODO Auto-generated method stub
				if(o1.getPathCost() < o2.getPathCost()) {
					return -1;
				}
				if(o1.getPathCost() >= o2.getPathCost()){
					return 1;
				}
//				if(o1.getPathCost()  == o2.getPathCost()) {
//					if(o1.getInputOrder() < o2.getInputOrder()){
//						return -1;
//					}
//					if(o1.getInputOrder() > o2.getInputOrder()){
//						return 1;
//					}
//				} 
				return 0;
			}
		});
		Iterator<Node> childIt;
		childNode = inputGraph.get(startState);
		ucsQueue.add(childNode);
		childNode = null;
//		System.out.println("Traverse:\n");
		while(!ucsQueue.isEmpty()){
			popNode = ucsQueue.remove();
			popNode.setVisited(true);
//			System.out.println(popNode.toString() + "\t" + popNode.getParent() + "\n");
			if(popNode.toString().equals(goalState)){
				goalNode = popNode;				
				while(goalNode != null){
					sb.insert(0, goalNode.toString()+" "+goalNode.getPathCost()+"\n");
					goalNode = goalNode.getParent();
				}
				return sb.toString();
			} else {
				pathCost = popNode.getPathCost();
				childIt = popNode.getChildrenIterator();
				while(childIt.hasNext()){
					childNode = childIt.next();
					stepCost = popNode.getStepCost(childNode);
					pathCostQueueNode = childNode.getPathCost();
					if(!childNode.isVisited() && !ucsQueue.contains(childNode)){
						childNode.setPathCost(pathCost + stepCost);
						childNode.addParent(popNode);
						ucsQueue.add(childNode);
					} else if(ucsQueue.contains(childNode)){						
						if((pathCost + stepCost) < pathCostQueueNode){
							ucsQueue.remove(childNode);
							childNode.resetParents();
							childNode.addParent(popNode);
							childNode.setPathCost(pathCost + stepCost);
							ucsQueue.add(childNode);
						}
					} else if(childNode.isVisited()){
						if((pathCost + stepCost) < pathCostQueueNode){
							childNode.setVisited(false);
							childNode.resetParents();
							childNode.addParent(popNode);
							childNode.setPathCost(pathCost + stepCost);
							ucsQueue.add(childNode);
						}
					}
				}
			}
		}
		return null;
	}
	
	public static String performAStar( String startState, String goalState, HashMap<String, Node> inputGraph){
		long pathCost = 0, pathCostQueueNode = 0, stepCost = 0;
		StringBuilder sb = new StringBuilder();
		Node popNode, childNode, goalNode;
		PriorityQueue<Node> aStarQueue = new PriorityQueue<>(new Comparator<Node>() {
			@Override
			public int compare(Node o1, Node o2) {
				// TODO Auto-generated method stub
				if(o1.getPathCost() + o1.getHeuristics() < o2.getPathCost() + o2.getHeuristics()) {
					return -1;
				} 
				if(o1.getPathCost() + o1.getHeuristics() >= o2.getPathCost() + o2.getHeuristics()) {
					return 1;
				}
//				if(o1.getPathCost() + o1.getHeuristics() == o2.getPathCost() + o2.getHeuristics()){
//					if(o1.getInputOrder() < o2.getInputOrder()){
//						return -1;
//					}
//					if(o1.getInputOrder() > o2.getInputOrder()){
//						return 1;
//					}
//				}
				return 0;
			}
		});
		Iterator<Node> childIt;
		childNode = inputGraph.get(startState);
		aStarQueue.add(childNode);
		childNode = null;
//		System.out.println("Traverse:\n");
		while(!aStarQueue.isEmpty()){
			popNode = aStarQueue.remove();
			popNode.setVisited(true);
//			System.out.println(popNode.toString() + "\t" + popNode.getParent() + "\n");
			if(popNode.toString().equals(goalState)){
				goalNode = popNode;				
				while(goalNode != null){
					sb.insert(0, goalNode.toString()+" "+goalNode.getPathCost()+"\n");
					goalNode = goalNode.getParent();
				}
				return sb.toString();
			} else {
				pathCost = popNode.getPathCost();
				childIt = popNode.getChildrenIterator();
				while(childIt.hasNext()){
					childNode = childIt.next();
					stepCost = popNode.getStepCost(childNode);
					pathCostQueueNode = childNode.getPathCost();
					if(!childNode.isVisited() && !aStarQueue.contains(childNode)){
						childNode.setPathCost(pathCost + stepCost);
						childNode.addParent(popNode);
						aStarQueue.add(childNode);
					} else if(aStarQueue.contains(childNode)){						
						if((pathCost + stepCost) < pathCostQueueNode){
							aStarQueue.remove(childNode);
							childNode.resetParents();
							childNode.addParent(popNode);
							childNode.setPathCost(pathCost + stepCost);
							aStarQueue.add(childNode);
						}
					} else if(childNode.isVisited()){
						if((pathCost + stepCost) < pathCostQueueNode){
							childNode.setVisited(false);
							childNode.resetParents();
							childNode.addParent(popNode);
							childNode.setPathCost(pathCost + stepCost);
							aStarQueue.add(childNode);
						}
					}
				}
			}
		}
		return null;
	}
}